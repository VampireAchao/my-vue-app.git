import http from '@/utils/httpRequest.js'

export function policy() {
    return new Promise((resolve, reject) => {
        http({
            url: '/oss/getMark',
            method: "get",
        }).then(({data}) => {
            resolve(data);
        })
    });
}