import Vue from 'vue'
import Router from 'vue-router'

// 解决路由重复问题
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err)
}

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {path: '*', redirect: {name: '404'}},
        {path: '/', redirect: {name: 'home'}},
        {path: '/404', name: '404', component: resolve => require(['../views/common/404.vue'], resolve)},
        {path: '/home', name: 'home', component: resolve => require(['../views/sys/home.vue'], resolve)}
    ]
})