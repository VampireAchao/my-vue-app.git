import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import httpRequest from '@/utils/httpRequest'
import VueCookie from 'vue-cookie';


Vue.use(ElementUI, httpRequest, VueCookie);
Vue.config.productionTip = false
// 挂载全局
Vue.prototype.$http = httpRequest
Vue.prototype.$cookie = VueCookie
new Vue({
    router,
    render: h => h(App),
}).$mount('#app')