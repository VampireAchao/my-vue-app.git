module.exports = {
    devServer: {
        // 配置端口
        port: 3000,
        // 不检查host
        disableHostCheck: true,
        // 配置代理
        proxy: {
            '/ruben': {
                // 配置URL
                target: process.env.VUE_APP_SERVER_URL,
                // 是否允许跨域
                changeOrigin: true,
                // 请求重写，这里是把ruben替换成空串
                pathRewrite: {
                    '^/ruben': ''
                }
            }
        }
    }

}